import React, { useEffect } from 'react';
import { makeStyles, createStyles, Box, Typography, Container } from '@material-ui/core';
import Allitems from './Allitems';
import { useSelector, useDispatch } from 'react-redux';
import { fetchCartData } from "./CartReducer/action";
import CardList from './CardList';
import Loader from '../../GlobalComponents/Loader';
import ShopAlert from '../../GlobalComponents/ShopAlert';


const Cart = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const cartData = useSelector(state => state.CartReducer.data) || [];
    const loading = useSelector(state => state.CartReducer.loading);
    const userId = localStorage.getItem('userID');
    
    useEffect(() => {
        if (cartData.length === 0 && userId) {
            dispatch(fetchCartData());
        }
    }, [cartData, dispatch,userId]);

    return (
        <div>
            <Container maxWidth="xl">
                {loading ? <Loader/> : cartData.length === 0 ? <ShopAlert/> :
                <div>
                    <div>
                        <Box className={classes.message}>
                            <Typography variant="h5" className={classes.Typography}>
                                Dear costumer we hereby notice you that,if  the price of the items is above RS2000 delivery charge will be free.
                            </Typography>
                        </Box>
                    </div>
                    <div className={classes.cart}>
                        <div className={classes.CardList}>
                            <CardList />
                        </div>
                        <div className={classes.items}>
                            <Allitems CartData={cartData}/>
                        </div>
                    </div>
                </div>
                }
            </Container>
        </div>



    )

}


const useStyles = makeStyles(theme =>
    createStyles({
        message: {
            // width: "1175px",
            padding: "8% 2% 2% 4%",
            margin: "3% 8% 0% 2%",
            [theme.breakpoints.between('md','lg')]: {
                padding: "3% 2% 2% 4%",
            },
            [theme.breakpoints.between('sm','sm')] : {
                padding: "8% 2% 2% 4%",
                margin:'3% 0 3% 2%'
            },
            [theme.breakpoints.down('xs')] : {
                padding: "10% 2% 2% 4%",
                margin:'9% 0 3% 2%'
            }
        },
        Typography: {
            fontSize: "23px",
            fontFamily: "inherit",
            fontWeight: "500",
            color: "#00669b",
            [theme.breakpoints.between('sm','md')]: {
                fontSize: '18px',
            },
            [theme.breakpoints.down('xs')]: {
                fontSize:'14px'
            }
        },
        cart: {
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-around",
            [theme.breakpoints.down('md')]: {
                flexDirection:'column-reverse',
                justifyContent:'space-between'
            }
        },

        items: {
            width: "fit-content",
            // marginLeft: "auto",
            marginTop: "4% 6% 0% 0%",
            [theme.breakpoints.down('md')]: {
                display:'flex',
                justifyContent:'center',
                width:'100%'
            }
        },
        CardList: {
            display: "flex",
            justifyContent: "space-between",
            margin: "2% 2% 1% 3%",
            [theme.breakpoints.down('md')]: {
                margin:'0'
            }
        },


    })
)



export default Cart;

