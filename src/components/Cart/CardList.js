import React from 'react';
// import { Grid } from '@material-ui/core';
import CartCard from './CartCard';
import { useSelector } from 'react-redux';
import {Grid,makeStyles, createStyles} from '@material-ui/core';


const CardList = (props) => {
  const cartData = useSelector(state => state.CartReducer.data) || [];
  const classes = useStyles();
  return (
    <Grid item md={12}>
      <Grid container spacing={3} classes={{container:classes.container}}>
        {cartData.map((item, index) => (
          <Grid item md={4} key={index}>
            <CartCard
              name={item.name}
              description={item.description}
              price={item.value}
              img={item.img}
              PostId={item.postId}
              id={item.id}
              quantity={item.quantity}
            />
          </Grid >
        ))}

      </Grid >

    </Grid>
  )
}
const useStyles = makeStyles(theme =>
  createStyles({
      item: {
          [theme.breakpoints.down('sm')]:{
              margin:'12px'
          },
          margin:'0'
      },
      container: {
          [theme.breakpoints.down('sm')]:{
              display:'flex',
              alignItems:'center',
              justifyContent:'center'
          },
          
      }
  })
)


export default CardList;