import React from 'react';
import ImageSlider from '../ImageSlider/ImageSlider';
import AppCardList from '../AppCard/AppCardList';
import { Typography,makeStyles,createStyles } from '@material-ui/core';
import HomeBlog from '../HomeBlog/HomeBlog';
import Connect from '../Connect/Connect';
import Footer from '../Footer/footer';

const Home = () =>{
    const classes = useStyles();
    return(
        <div>
            <ImageSlider/>
            <div className={classes.CardTop}>
                <div className={classes.Typography}>
                    <Typography className={classes.Typo}>"With our exclusive collection be ready for big day"</Typography>
                </div>
                <AppCardList/> 
            </div>           
            <HomeBlog/>
            <Connect/>
            <Footer/>
        </div>
    )
}

const useStyles = makeStyles(theme =>
    createStyles({
        CardTop:{
            padding:'2% 6%  2% 11%',
            [theme.breakpoints.between('md','md')]: {
                padding:'2% 4% 2% 4%'
            },
            [theme.breakpoints.between('sm','sm')]: {
                padding:'2% 0'
            },
            [theme.breakpoints.between('sm','sm')]: {
                padding:'2% 9%'
            },

        },
        Typography: {
            display:'flex',
            justifyContent:'center',
            padding:'3%',
            [theme.breakpoints.down('md')]: {
                padding:'2%'
            }
        },
        Typo: {
            fontSize:'19px',
            fontFamily:'cursive',
            [theme.breakpoints.down('md')]: {
                fontSize: '16px',
            }
        }
    }))


export default Home;