import React, { useState, useEffect } from "react";
import {
  makeStyles,
  createStyles,
  Button,
  AppBar,
  Toolbar,
} from "@material-ui/core";
import logo from "./logo.png";
import { Link } from "react-router-dom";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import { useSelector, useDispatch } from "react-redux";
import Account from '../Account/Account';
import NavigationItem from './NavigationItem/NavigationItem';
import { authCheckState } from '../Authentication/AuthReducer/action';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SideDrawer from "../SideDrawer/SideDrawer";


const NavigationBar = () => {

  const [open, setOpen] = useState(false);
  const [anchorEl, setAnchorEl] = useState(null);
  const [openSlider,setOpenSlider] = useState(false);

  const classes = useStyles();
  const dispatch = useDispatch();
  const token = useSelector((state) => state.AuthReducer.refreshToken !== null);

  const handleOpenMenu = (event) => {
    if (!open) {
      setOpen(true);
      setAnchorEl(event.currentTarget)
    } else {
      setOpen(false);
      setAnchorEl(null);
    }

  }

  const handleCloseMenu = () => {
    setOpen(false);
    setAnchorEl(null);
  }

  const closeSlidebarHandler = () => {
    setOpenSlider(false);
  }

  const openSliderHandler = () => {
    setOpenSlider(true);
  }

  useEffect(() => {
    dispatch(authCheckState());
  }, [dispatch])

  return (
    <AppBar position="fixed" color="primary" elevation={0}>
      <Toolbar className={classes.mobDesign}>

        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu" onClick={openSliderHandler}>
          <MenuIcon />
        </IconButton>

            <Link to={"/"}>
              <img src={logo} alt="logo" className={classes.logo} />
            </Link>
            {token && 
            <Button classes={{ text: classes.text }} onClick={handleOpenMenu}>
              <AccountCircleIcon fontSize="large" style={{ color: 'white' }} />
              <Account open={open} handleClose={handleCloseMenu} anchorEl={anchorEl} />
            </Button> 
          }
      </Toolbar>
      <div className={classes.navBar}>
        <Toolbar>
          <div style={{ paddingRight: "9%" }}>
            <Link to={"/"}>
              <img src={logo} alt="logo" className={classes.logo} />
            </Link>
          </div>
          <NavigationItem to={"/"} exact>
            Home
          </NavigationItem>
          <NavigationItem to={"/Shop"}>
            Shop
          </NavigationItem>
          <NavigationItem to={"/Blog"}>
            Blog
          </NavigationItem>
          <NavigationItem to={"/AboutUS"}>
            About US
          </NavigationItem>
        </Toolbar>
      <Toolbar>
      <div>
          {token ? (
            <Button classes={{ text: classes.text }} onClick={handleOpenMenu}>
              <AccountCircleIcon fontSize="large" style={{ color: 'white' }} />
              <Account open={open} handleClose={handleCloseMenu} anchorEl={anchorEl} />
            </Button>
          ) : (
              <NavigationItem to={"/Authentication"}>
                Authentication
              </NavigationItem>
            )}
        </div>
        <NavigationItem icon to={"/Cart"}>
          <ShoppingCartIcon style={{ paddingTop: '8px' }} fontSize="large" />
        </NavigationItem>
      </Toolbar>
      </div>
      <SideDrawer open={openSlider} onClose={closeSlidebarHandler} token={token}/>
    </AppBar>
  );
};

const useStyles = makeStyles((theme) =>
  createStyles({
    mobDesign:{
      display:'none',
      [theme.breakpoints.down('xs')]: {
        display:'flex',
        justifyContent:'space-between'
      }
    },
    navBar: {
      display: "flex",
      height: "54px",
      justifyContent: "space-between",
      alignItems: "center",
      flexGrow: '1',
      padding: "0% 8%",
      [theme.breakpoints.down('sm','md')]: {
        justifyContent:'center'
      },
      [theme.breakpoints.down('xs')]: {
        display:'none'
      }
    },
    menuButton: {
      marginRight: theme.spacing(2),
    },
    SearchBox: {
      width: '100%',
      display: 'flex',
      justifyContent: 'flex-end'
    },
    text: {
      padding: "6px 11px",
      textTransform: "initial",
      fontSize: "16px",
    },
    logo: {
      maxWidth: 50,
    }
  })
);

export default NavigationBar;
