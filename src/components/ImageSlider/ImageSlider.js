import React from 'react';
import { Zoom } from 'react-slideshow-image';
import toychild from '../../Asset/ImageSlider/toychild.jpg';
import mother from '../../Asset/ImageSlider/mother.jpg';
import babe from '../../Asset/ImageSlider/babe.jpg';
import { makeStyles,createStyles,Typography,Button } from '@material-ui/core';
import { useHistory } from 'react-router-dom';
// import classes from '*.module.css';
 
const zoomOutProperties = {
  duration: 9000,
  transitionDuration: 500,
  infinite: true,
  indicators: true,
  scale: 0.4,
  arrows: true
}
 
const ImageSlider = () => {
  const classes = useStyles();
  const history = useHistory();

  const data = [
    {img:babe,className:classes.babe, typo:'Pick the product of your desire with exclusive discount',button:'Shop'},
    {img:mother,className:classes.mother,typo:'Read about how you could care best for your child', button:'Blog'},
    {img:toychild,className:classes.toychild,typo:'Checkout your product and enjoy our exclusive product', button:'Cart'},
  ]

  const handleNavigation = (nav) => {
    history.push(`/${nav}`)
  }

    return (
      <div className={classes.slide}>
        <Zoom {...zoomOutProperties}>
          {
            data.map((item,index) => (
              <div key={index} style={{position:'relative'}}>
                { item.img === babe ?                   
                <img className={classes.img2} alt="img slider 2" src={item.img} /> : 
                <img className={classes.img1} alt="img slider 1" src={item.img} />
                }
                  <div className={item.className}>
                    <Typography variant="h5" className={classes.Typography}>{item.typo}</Typography>
                    <Button className={classes.Button} onClick={() => handleNavigation(item.button)}>{item.button}</Button>
                  </div>
              </div>
            ))
          }
        </Zoom>
      </div>
    )
}
const useStyles = makeStyles(theme => 
  createStyles({
    mother: {
        position: 'absolute',
        bottom:'35%',
        right:'10%',
        
    },
    slide: {
      paddingTop:'2%'
    },
    img1: {
      height:'520px', 
      width:'100%',
      [theme.breakpoints.down('md')]: {
        height:'400px',
        width:'100%'
      }
    },
    img2:{
      height:'520px',
      width:'150%',
      [theme.breakpoints.down('md')]: {
        height:'400px',
        width:'100%'
      }
    },
    toychild: {
      position: 'absolute',
      bottom:'20%',
      left:'10%',
      [theme.breakpoints.down('md')]: {
        bottom:'30%'
      }
    },
    babe: {
      position: 'absolute',
      bottom:'30%',
      right:'40%',
      [theme.breakpoints.down('xs')]: {
        right:'10%'
      }
    },
    Typography: {
      color:'white',
      letterSpacing:'1px',
      paddingBottom:'2%',
      [theme.breakpoints.down('md')]: {
        fontSize:'16px'
      }
    },
    Button: {
      background:'#373b38',
      width:'130px',
      textTransform:'initial',
      borderRadius:'9px',
      height:'50px',
      color:'white',
      fontSize:'18px',
      textAlign:'center',
      letterSpacing:'2px',
      "&:hover": {
        background:'#5b635d'
      },
      [theme.breakpoints.down('md')]: {
        width:'90px',
        height:'40px',
        borderRadius:'4px',
        fontSize:'14px'
      }
    }
  }))
export default ImageSlider;
