import React from 'react';
import { Drawer,Box } from '@material-ui/core';
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import NavigationItem from '../NavigationBar/NavigationItem/NavigationItem';

const SideDrawer = (props) =>{
    const { open,onClose,token} = props;
    // const classes = useStyles();
    return (
        <Box>
            <Drawer
                open={open}
                onClose={onClose}
                // style={{width:'350px'}}
            >
                <NavigationItem to={"/"} exact>
                    Home
                </NavigationItem>
                <NavigationItem to={"/Shop"}>
                    Shop
                </NavigationItem>
                <NavigationItem to={"/Blog"}>
                    Blog
                </NavigationItem>
                <NavigationItem to={"/AboutUS"}>
                    About US
                </NavigationItem>
                {
                !token && 
                <NavigationItem to={"/Authentication"}>
                    Authentication
                </NavigationItem>
                }
               
                <NavigationItem icon to={"/Cart"}>
                    <ShoppingCartIcon style={{ paddingTop: '8px' }} fontSize="large" />
                </NavigationItem>

            </Drawer>
        </Box>
    )
}

export default SideDrawer;