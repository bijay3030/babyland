import { combineReducers } from "redux";
import AuthReducer from "../components/Authentication/AuthReducer/AuthReducer";
import CosmeticReducer from "../components/Shop/ShopLists/Cosmetic/CosmeticReducer/CosmeticReducer";
import DiaperReducer from "../components/Shop/ShopLists/Diaper/DiaperReducer/DiaperReducer";
import FoodReducer from "../components/Shop/ShopLists/Foods/FoodReducer/FoodReducer";
import PregnancykitsReducer from "../components/Shop/ShopLists/PregnancyKits/PregnancykitsReducer/PregnancykitsReducer";
import ShampooReducer from "../components/Shop/ShopLists/Shampoo/ShampooReducer/ShampooReducer";
import ToysReducer from "../components/Shop/ShopLists/Toys/ToysReducer/ToysReducer";
import TShirtReducer from "../components/Shop/ShopLists/Tshirt/TShirtReducer/TShirtReducer";
import WishlistReducer from '../components/Account/Wishlist/WishlistReducer/WishlistReducer';
import CartReducer from '../components/Cart/CartReducer/CartReducer';
import ProfileReducer from '../components/Account/Profile/ProfileReducer/ProfileReducer';
import OrderReducer from "../components/Account/Order/OrderReducer/OrderReducer";
import DeveloperReducer from '../components/AboutUs/Developer/DeveloperReducer/DeveloperReducer';


const rootReducer = combineReducers({
  AuthReducer,
  CosmeticReducer,
  DiaperReducer,
  FoodReducer,
  PregnancykitsReducer,
  ShampooReducer,
  ToysReducer,
  TShirtReducer,
  WishlistReducer,
  CartReducer,
  ProfileReducer,
  OrderReducer,
  DeveloperReducer
});

export default rootReducer;
