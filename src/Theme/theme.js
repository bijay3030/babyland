import { createMuiTheme } from '@material-ui/core';


const theme = createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 650,
      md: 968,
      lg: 1280,
      xl: 1920,
    },
  },
    palette: {
        primary: {
          main:'#f51bb3'
        },
        secondary: {
          main:'#f5790c'
        },
        // text: {
        //   primary:'white'  
        // }
    },
    overrides: {
      MuiButton: {
        root: {
          color: 'Black',
          // backgroundColor:'hotpink',
          fontSize:'14px'
          // '&:hover': {
          //   backgroundColor: ''
          // }
        }
      }},
      
});
export default theme;